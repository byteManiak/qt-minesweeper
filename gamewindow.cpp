#include "gamewindow.h"
#include "ui_gamewindow.h"

void GameWindow::generateMines(std::vector<Tile*> &tiles)
{
	std::mt19937 rng(unsigned(time(nullptr)));
	std::uniform_int_distribution<unsigned int> xRand(0, gridProp.width-1);
	std::uniform_int_distribution<unsigned int> yRand(0, gridProp.height-1);

	int i = 0;
	while(i < gridProp.numMines)
	{
		unsigned x = xRand(rng), y = yRand(rng);
		if(tiles[x + y*gridProp.width]->val == "*") continue;
		else { i++; tiles[x + y*gridProp.width]->val = "*"; }
	}
}

void increaseVal(Tile* t)
{
	QString val = t->val;
	if(t->val == "*") return;
	unsigned a = val.toUInt();
	t->val = QString::number(a+1);
}

void GameWindow::generateNums(std::vector<Tile*> &tiles)
{
	for(int y = 0; y < gridProp.height; y++)
		for(int x = 0; x < gridProp.width; x++)
		{
			if(tiles[x+y*gridProp.width]->val == "*")
			{
				Tile *t;
				if(y > 0)
				{
					t = tiles[x+(y-1)*gridProp.width];
					increaseVal(t);
					if(x > 0)
					{
						t = tiles[(x-1) + (y-1)*gridProp.width];
						increaseVal(t);
					}
					if(x < gridProp.width-1)
					{
						t = tiles[(x+1) + (y-1)*gridProp.width];
						increaseVal(t);
					}
				}
				if(y < gridProp.height-1)
				{
					t = tiles[x + (y+1)*gridProp.width];
					increaseVal(t);
					if(x > 0)
					{
						t = tiles[(x-1) + (y+1)*gridProp.width];
						increaseVal(t);
					}
					if(x < gridProp.width-1)
					{
						t = tiles[(x+1) + (y+1)*gridProp.width];
						increaseVal(t);
					}
				}
				if(x > 0)
				{
					t = tiles[(x-1) + y*gridProp.width];
					increaseVal(t);
				}
				if(x < gridProp.width-1)
				{
					t = tiles[(x+1) + y*gridProp.width];
					increaseVal(t);
				}
			}
		}
}

GameWindow::GameWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::GameWindow)
{
	layout = new QGridLayout();
	layout->setSpacing(0);
	ui->setupUi(this);
	ui->centralWidget->setLayout(layout);

	layout->setSizeConstraint(QLayout::SetFixedSize);

	gameover = new QMessageBox();
	gameover->setText("You lost");
	gameover->setWindowTitle("Game over");

	win = new QMessageBox();
	win->setText("You win");
	win->setWindowTitle("Game won");

	flagsLabel = new QLabel("Flags: " + QString::number(flags));
	ui->statusBar->addWidget(flagsLabel);

	connect(gameover, SIGNAL(buttonClicked(QAbstractButton*)), this, SLOT(newgame()));
	connect(win, SIGNAL(buttonClicked(QAbstractButton*)), this, SLOT(newgame()));
	connect(ui->actionNew, SIGNAL(triggered(bool)), this, SLOT(newgame()));

	connect(ui->actionBeginner, SIGNAL(triggered(bool)), this, SLOT(newDifficulty()));
	connect(ui->actionIntermediate, SIGNAL(triggered(bool)), this, SLOT(newDifficulty()));
	connect(ui->actionExpert, SIGNAL(triggered(bool)), this, SLOT(newDifficulty()));

	newgame();

	this->resize(layout->sizeHint());
	this->setFixedSize(this->minimumWidth(), this->minimumHeight());
}

void GameWindow::uncoverTile(int x, int y, bool canUncoverMine)
{
	if(x < 0 || x > gridProp.width-1 || y < 0 || y > gridProp.height-1) return;

	Tile *t = tiles[x + y*gridProp.width];
	if(t->text() == "F") return;
	t->setChecked(true);

	if(t->uncovered) return;
	t->setText(t->val);
	t->uncovered = true;

	if(t->val == "*" && !canUncoverMine)
	{
		uncoverAllTiles();
		gameover->show();
	}

	if(t->val != " ") return;
	else
	{
		uncoverTile(x, y-1, canUncoverMine);
		uncoverTile(x, y+1, canUncoverMine);
		uncoverTile(x-1, y-1, canUncoverMine);
		uncoverTile(x-1, y+1, canUncoverMine);
		uncoverTile(x+1, y-1, canUncoverMine);
		uncoverTile(x+1, y+1, canUncoverMine);
		uncoverTile(x+1, y, canUncoverMine);
		uncoverTile(x-1, y, canUncoverMine);
	}
}

void GameWindow::uncoverAllTiles()
{
	for(int x = 0; x < gridProp.width; x++)
		for(int y = 0; y < gridProp.height; y++)
			uncoverTile(x, y, true);
}

void GameWindow::uncover()
{
	Tile* t = qobject_cast<Tile*>(sender());

	int x = t->x, y = t->y;
	if (!t->isChecked())
		uncoverTile(x, y, false);
	else
		uncoverAround();

	int safes = 0;
	for(auto i : tiles) if (i->isChecked()) safes++;

	if (safes == gridProp.width*gridProp.height-gridProp.numMines)
	{
		uncoverAllTiles();
		win->show();
	}
}

void GameWindow::flag()
{
	Tile* t = qobject_cast<Tile*>(sender());
	if(t->uncovered) return;

	if(t->text() == "F"){ t->setText(" "); flags++; }
	else { t->setText("F"); t->setChecked(false); flags--; }

	flagsLabel->setText("Flags:" + QString::number(flags));

	int bombs = 0;
	for(auto i : tiles) if(i->text() == "F" && i->val == "*") bombs++;

	if(bombs == gridProp.numMines)
	{
		uncoverAllTiles();
		win->show();
	}
}

bool GameWindow::isFlagged(int x, int y)
{
	if(x < 0 || x > gridProp.width-1 || y < 0 || y > gridProp.height-1)
		return 0;

	Tile *t = tiles[x + y*gridProp.width];
	return t->text() == "F" ? 1 : 0;
}

void GameWindow::uncoverAround()
{
	Tile *t = qobject_cast<Tile*>(sender());

	// If tile was not yet uncovered, don't do anything
	if (!t->isChecked()) return;

	int x = t->x, y = t->y;

	int total = 0;
	total += isFlagged(x, y-1);
	total += isFlagged(x, y+1);
	total += isFlagged(x-1, y);
	total += isFlagged(x+1, y);
	total += isFlagged(x-1, y-1);
	total += isFlagged(x+1, y-1);
	total += isFlagged(x-1, y+1);
	total += isFlagged(x+1, y+1);
	if(total == 0) return;
	if(total == t->val.toInt())
	{
		if(!isFlagged(x, y-1)) uncoverTile(x, y-1, false);
		if(!isFlagged(x, y+1)) uncoverTile(x, y+1, false);
		if(!isFlagged(x-1, y-1)) uncoverTile(x-1, y-1, false);
		if(!isFlagged(x-1, y+1)) uncoverTile(x-1, y+1, false);
		if(!isFlagged(x+1, y-1)) uncoverTile(x+1, y-1, false);
		if(!isFlagged(x+1, y+1)) uncoverTile(x+1, y+1, false);
		if(!isFlagged(x+1, y)) uncoverTile(x+1, y, false);
		if(!isFlagged(x-1, y)) uncoverTile(x-1, y, false);
	}
}

void GameWindow::newDifficulty()
{
	QAction *difficulty = qobject_cast<QAction*>(sender());

	if (difficulty->text() == "Beginner")
	{
		gridProp = gridProps[BEGINNER];
		newgame();
	}
	else if (difficulty->text() == "Intermediate")
	{
		gridProp = gridProps[INTERMEDIATE];
		newgame();
	}
	else
	{
		gridProp = gridProps[EXPERT];
		newgame();
	}
}

void GameWindow::newgame()
{
	flags = gridProp.numMines;
	flagsLabel->setText("Flags: " + QString::number(flags));

	for(auto &i : tiles) delete(i);
	tiles.erase(tiles.begin(), tiles.end());

	for(int y = 0; y < gridProp.height; y++)
		for(int x = 0; x < gridProp.width; x++)
		{
			tiles.emplace_back(new Tile(x, y));
			Tile *&currentTile = tiles[x + y*gridProp.width];
			currentTile->setGeometry(y*24, x*24, 24, 24);
			currentTile->setMinimumSize(24, 24);
			currentTile->setMaximumSize(24, 24);
			currentTile->setStyleSheet("font: bold 14px");
			layout->addWidget(currentTile, y, x);
			connect(currentTile, SIGNAL(pressed()), this, SLOT(uncover()));
			connect(currentTile, SIGNAL(rightClicked()), this, SLOT(flag()));
			connect(currentTile, SIGNAL(middleClicked()), this, SLOT(uncoverAround()));
		}

	generateMines(tiles);
	generateNums(tiles);

	ui->centralWidget->adjustSize();
	adjustSize();
}

GameWindow::~GameWindow()
{
	for(auto &i : tiles) delete(i);
	tiles.erase(tiles.begin(), tiles.end());
	delete gameover;
	delete win;
	delete flagsLabel;
	delete layout;
	delete ui;
}
