/********************************************************************************
** Form generated from reading UI file 'gamewindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GAMEWINDOW_H
#define UI_GAMEWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_GameWindow
{
public:
    QAction *actionNew;
    QAction *actionQuit;
    QAction *actionBeginner;
    QAction *actionIntermediate;
    QAction *actionExpert;
    QWidget *centralWidget;
    QMenuBar *menuBar;
    QMenu *menuGame;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *GameWindow)
    {
        if (GameWindow->objectName().isEmpty())
            GameWindow->setObjectName(QStringLiteral("GameWindow"));
        GameWindow->resize(400, 300);
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(GameWindow->sizePolicy().hasHeightForWidth());
        GameWindow->setSizePolicy(sizePolicy);
        actionNew = new QAction(GameWindow);
        actionNew->setObjectName(QStringLiteral("actionNew"));
        actionQuit = new QAction(GameWindow);
        actionQuit->setObjectName(QStringLiteral("actionQuit"));
        actionBeginner = new QAction(GameWindow);
        actionBeginner->setObjectName(QStringLiteral("actionBeginner"));
        actionIntermediate = new QAction(GameWindow);
        actionIntermediate->setObjectName(QStringLiteral("actionIntermediate"));
        actionExpert = new QAction(GameWindow);
        actionExpert->setObjectName(QStringLiteral("actionExpert"));
        centralWidget = new QWidget(GameWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        sizePolicy.setHeightForWidth(centralWidget->sizePolicy().hasHeightForWidth());
        centralWidget->setSizePolicy(sizePolicy);
        GameWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(GameWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 400, 22));
        menuGame = new QMenu(menuBar);
        menuGame->setObjectName(QStringLiteral("menuGame"));
        GameWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(GameWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        GameWindow->setStatusBar(statusBar);

        menuBar->addAction(menuGame->menuAction());
        menuGame->addAction(actionNew);
        menuGame->addSeparator();
        menuGame->addAction(actionBeginner);
        menuGame->addAction(actionIntermediate);
        menuGame->addAction(actionExpert);
        menuGame->addSeparator();
        menuGame->addAction(actionQuit);

        retranslateUi(GameWindow);
        QObject::connect(actionQuit, SIGNAL(triggered()), GameWindow, SLOT(close()));

        QMetaObject::connectSlotsByName(GameWindow);
    } // setupUi

    void retranslateUi(QMainWindow *GameWindow)
    {
        GameWindow->setWindowTitle(QApplication::translate("GameWindow", "QtSweeper", Q_NULLPTR));
        actionNew->setText(QApplication::translate("GameWindow", "New", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionNew->setShortcut(QApplication::translate("GameWindow", "F2", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionQuit->setText(QApplication::translate("GameWindow", "Quit", Q_NULLPTR));
        actionBeginner->setText(QApplication::translate("GameWindow", "Beginner", Q_NULLPTR));
        actionIntermediate->setText(QApplication::translate("GameWindow", "Intermediate", Q_NULLPTR));
        actionExpert->setText(QApplication::translate("GameWindow", "Expert", Q_NULLPTR));
        menuGame->setTitle(QApplication::translate("GameWindow", "Game", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class GameWindow: public Ui_GameWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GAMEWINDOW_H
