#include "tile.h"

Tile::Tile(int x, int y, QWidget *parent) : QPushButton{parent}, x{x}, y{y}
{
	val = " ";
	setCheckable(true);
}

void Tile::mousePressEvent(QMouseEvent *e)
{
	if(e->button() == Qt::LeftButton) emit pressed();
	if(e->button() == Qt::RightButton) emit rightClicked();
	if(e->button() == Qt::MiddleButton) emit middleClicked();
}
