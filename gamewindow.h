#ifndef GAMEWINDOW_H
#define GAMEWINDOW_H

#include <random>
#include <QMessageBox>
#include <QGridLayout>
#include <QMainWindow>
#include <QStatusBar>
#include <QLabel>
#include <QAction>
#include "tile.h"

namespace Ui
{
class GameWindow;
}

#define BEGINNER 0
#define INTERMEDIATE 1
#define EXPERT 2

struct GridProp { int width, height, numMines; };

const GridProp gridProps[] =
{
	{9, 9, 10},
	{16, 16, 40},
	{30, 16, 99}
};

class GameWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit GameWindow(QWidget *parent = nullptr);
	~GameWindow();

public slots:
	void flag();
	void uncoverAround();
	void newgame();
	void uncover();
	void newDifficulty();

private:
	bool isFlagged(int x, int y);
	void uncoverTile(int x, int y, bool canUncoverMine);
	void uncoverAllTiles();
	void generateNums(std::vector<Tile*> &tiles);
	void generateMines(std::vector<Tile*> &tiles);

	int flags = 40;
	QLabel *flagsLabel;
	QMessageBox *gameover;
	QMessageBox *win;
	QGridLayout *layout;
	std::vector<Tile*> tiles;
	Ui::GameWindow *ui;
	GridProp gridProp = gridProps[EXPERT];
};

#endif // GAMEWINDOW_H
