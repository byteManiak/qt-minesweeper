#ifndef TILE_H
#define TILE_H

#include <QDebug>
#include <QWidget>
#include <QPushButton>
#include <QMouseEvent>

class Tile : public QPushButton
{
	Q_OBJECT
public:
	Tile(int x, int y, QWidget *parent = nullptr);
	QString val;
	int x, y;
	bool uncovered = false;

public slots:
	void mousePressEvent(QMouseEvent *e);

signals:
	void rightClicked();
	void middleClicked();
};

#endif // TILE_H
